#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "algorithm.h"
#include "performance.h"

void performance(node *list) {
    node *n = list;
    int len;
    
    printf("input wanted array size:");
    scanf("%d", &len);
    int *temp = (int *) calloc(len, sizeof(int));
    int *array = (int *) calloc(len, sizeof(int));

    for(int i = 0; i < MAXRUNTIME; i++) {
        array = testarray(len);
        do {
            memcpy(temp, array, sizeof(int *));
            *(n->rtime + i) = (*(n->func))(temp, len);
            n = n->next;
        } while(n->name != list->name);
    }
}

