#include<stdio.h>
#include<stdlib.h>

#include "performance.h"

void favg(node *list) {
    FILE *perf = fopen("perf.csv", "a");
    node *n = list;

    do {// first colum
        fprintf(perf, "%s,", n->name);
        n = n->next;
    } while(n->name != list->name);
    fprintf(perf, "\n");

    //print every case
    for(int i = 0; i < MAXRUNTIME; i++) {
        do {//every case
            fprintf(perf, "%lf,", *(n->rtime + i));
            n = n->next;
        } while(n->name != list->name);
        fprintf(perf, "\n");
    }
    fclose(perf);
}//need to change into round linked list
