#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include "performance.h"

node *create(char *algorithm, double (*function)(int *, int)) {
    node *list = (node *)calloc(1, sizeof(node));
    list->name = algorithm;
    list->func = function;
    list->rtime = (double *)calloc(MAXRUNTIME + 1, sizeof(double));
    list->next = list;//have problem
    return list;
}

void addlist(node *n1, node *n2) {
    n2->next = n1->next;
    n1->next = n2;//just need to add to last
}
