#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include "algorithm.h"

void merge_sort(int *array, int *reg, int left, int right) {
    if (left >= right)
		return;
	int len = right - left, mid = (len >> 1) + left;
	int left1 = left, right1 = mid;
	int left2 = mid + 1, right2 = right;
	merge_sort(array, reg, left1, right1);
	merge_sort(array, reg, left2, right2);
	int k = left;
	while (left1 <= right1 && left2 <= right2)
		reg[k++] = array[left1] < array[left2] ? array[left1++] : array[left2++];
	while (left1 <= right1)
		reg[k++] = array[left1++];
	while (left2 <= right2)
		reg[k++] = array[left2++];
	for (k = left; k <= right; k++)
		array[k] = reg[k];
}
double merge(int *array, int len) {
	int reg[len];
    clock_t start, end;
	start = clock();
	
    merge_sort(array, reg, 0, len - 1);
    
    end = clock();
    return (end - start)/(double)(CLOCKS_PER_SEC);
}