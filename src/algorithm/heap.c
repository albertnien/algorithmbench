#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include "algorithm.h"

void node(int *array, int left, int right) {
	int parent = left;
	int child = parent * 2 + 1;
	while (child <= right) { 
		if (child + 1 <= right && array[child] < array[child + 1])
			child++;
		if (array[parent] > array[child])
			return;
		else {
			swap(&array[parent], &array[child]);
			parent = child;
			child = parent * 2 + 1;
		}
	}
}

double heap(int *array, int len) {
	int i;
    clock_t start, end;
	start = clock();
    for (i = len / 2 - 1; i >= 0; i--)
		node(array, i, len - 1);
	for (i = len - 1; i > 0; i--) {
		swap(&array[0], &array[i]);
		node(array, 0, i - 1);
	}
    end = clock();

    return (end - start)/(double)(CLOCKS_PER_SEC);
}