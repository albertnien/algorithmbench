#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include "algorithm.h"

void quick_sort(int *array, int left, int right)
{
    int i = left,   \
        j = right - 1,  \
        pivot = array[right];

	if (i >= j) return;
	while (i < j) {
		while (array[i] < pivot && i < j)
			i++;
		while (array[j] >= pivot && i < j)
			j--;
		swap(&array[i], &array[j]);
	}
	if (array[i] >= array[right])
		swap(&array[i], &array[right]);
	else
		i++;
    if (i) {
        quick_sort(array, left, i - 1);
    }
    quick_sort(array, i + 1, right);
}

double quick(int *array, int len)
{
	clock_t start, end;
	start = clock();
	
	quick_sort(array, 0, len - 1);
	
	end = clock();
    return (end - start)/(double)(CLOCKS_PER_SEC);
}
