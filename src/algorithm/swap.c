#include<stdio.h>

#include "algorithm.h"

void swap(int *a, int *b) 
{
    int temp = *a;
    *a = *b;
    *b = temp;
}