#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include "algorithm.h"

double insertion(int *array, int len) 
{
    clock_t start, end;
    int i, j, temp;

    start = clock();

    for(i = 1; i < len; i++) {
        temp = array[i];
        j= i - 1;
        for(;j >= 0 && array[j] > temp; j--){
 			array[j + 1] = array[j];
 	    }
 	    array[j + 1] = temp; 
    }

    end = clock();

    return (end - start)/(double)(CLOCKS_PER_SEC);
}
