#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include "algorithm.h"

double selection(int* array, int len)
{
    clock_t start, end;
    int i, j, min;

    start = clock();

    for(i = 0; i < len; i++) {
        min = i;
        for(j = i + 1; j < len + 1; j++) {
            if(array[min] > array[j]) {
                min = j;
            }
        }
        swap(&array[min], &array[i]);
    }

    end = clock();

    return (end - start)/(double)(CLOCKS_PER_SEC);
}