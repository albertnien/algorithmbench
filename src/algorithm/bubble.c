#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include "algorithm.h"

double bubble(int *array, int len)
{
    clock_t start, end;
    int i, j;

    start = clock();
    
    for(i = len; i > 0; i--) {
        for(j = 0; j < i - 1; j++) {
            if(array[j] > array[j + 1]) {
                swap(&array[j], &array[j + 1]);
            }
        }
    }

    end = clock();

    return (end - start)/(double)(CLOCKS_PER_SEC);
}
