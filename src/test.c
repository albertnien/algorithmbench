#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<stdlib.h>

#include "performance.h"

int *testarray(int len) {
    int *array = (int *)calloc(len, sizeof(int));
    srand((unsigned)time(NULL));
    for(;len >= 0; len--) {
        array[len] = rand();
    }
    return array;
}
