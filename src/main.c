#include<stdio.h>
#include<stdlib.h>
#include<stdlib.h>

#include "performance.h"

double quick(int*, int);
double selection(int*, int);
double insertion(int*, int);
double bubble(int*, int);
double merge(int*, int);
double heap(int*, int);

int main()
{
    int num = 0;

    node *head = create("quick", quick);
    node *temp = NULL;
    node *pre = head;

    do {
        scanf("%d", &num);
        printf("input : %d\n",num);
        switch(num) {
            case 1:
                temp = create("bubble", bubble);
                break;
            case 2:
                temp = create("insertion", insertion);
                break;
            case 3:
                temp = create("selection", selection);
                break;
            case 4:
                temp = create("merge", merge);
                break;
            default :
                performance(head);
                favg(head);
                goto EXIT;
        }
        addlist(pre, temp);
        pre = pre->next;
    } while(1);//end of select function to put in
EXIT:
    return 0;
}
