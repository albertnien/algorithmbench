Build a program to benchmark algorithms

Test platform:
- Intel core i3 - 2100
- DDR3 1333 4G * 2
- WD Blue 1T
- ubuntu 16.04 LTS (64-bit)

## Included algorithm
### Sort
- quick sort
- bubble sort
- insertion sort
- selection sort
- merge sort
- heap sort


