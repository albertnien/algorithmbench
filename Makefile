
CC = gcc
INC = -I ./include
OBJ = src/*.c src/algorithm/*.c

bench: ${OBJ}
	$(CC) -o $@ ${OBJ} ${INC} 
