#ifndef _PERFORMANCE_H_
#define _PERFORMANCE_H_

#define MAXRUNTIME 25

typedef struct nodeList {
    char *name;
    double (*func)(int *, int);
    double *rtime;
    struct nodeList *next;
} node;

void performance(node *);
int *testarray(int);
void favg(node *);

//linked list function
node *create(char *, double (*function)(int *, int));
void addlist(node *, node *);
#endif