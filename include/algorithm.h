#ifndef _ALGORITHM_H_
#define _ALGORITHM_H_

double quick(int*, int);

double selection(int*, int);

double insertion(int*, int);

double bubble(int*, int);

double merge(int*, int);

double heap(int*, int);

void swap(int*, int*);

#endif